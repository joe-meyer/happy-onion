# Happy Onion Discord Bot

The Happy Onion Discord Bot is a simple bot built to make Onion happy. 

## How to Run

This project uses [Npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm). You will need to have this installed on the machine running the application.

1. Clone the project
2. Run `npm install`
3. To create your own bot running this code, you will need to create a .env file at the root of the project and fill in the following contents:
    ```
    DISCORD_TOKEN=XXXX
    TEST_GUILD_ID=XXXX
    DISCORD_PUBLIC_KEY=XXXX
    DISCORD_APP_ID=XXXX
    ```
    TEST_GUILD_ID is optional, however registering this will allow for commands to be registered to that guild/server immediately when the code is executed instead of waiting on the gateway to propogate newly added commands.

    The other items can be obtained from the discord developer dashboard [creating a discord application](https://discord.com/developers/docs/getting-started#creating-an-app) and configuring a bot.

4. To start the application you can either use visual studio code launch.json to start debugging, or you can use the `npm run start` command, which will build and run the solution.


## Contributing

Contribution guidelines coming soon... maybe...

Currently there are two versions of this bot which are deployed as part of the pipeline on this repository, Sad Onion (Staging) and Happy Onion (Production). These bots can be exercised when code is merged and deployed and are available in the Test discord server: https://discord.gg/vt3Z75x9bd