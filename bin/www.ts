import { Application } from "../services/application";

// Require the necessary discord.js classes
const { Client, Intents } = require('discord.js');
require('dotenv').config('../.env');
const { REST } = require('@discordjs/rest');

const token = process.env.DISCORD_TOKEN;

// Create a new client instance
const myIntents = new Intents();
myIntents.add(Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MEMBERS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_MESSAGE_REACTIONS);
const client = new Client({ intents: myIntents });
const rest = new REST({ version: '9' }).setToken(token);

const application : Application = new Application(rest, client);


require('../commands/commands')(application)

// When the client is ready, run this code (only once)
application.client.once('ready', async () => {
	await application.registerCommands();
});

application.client.login(token);
