rm -rf ${DEPLOY_PATH}_backup
rsync -a --stats --mkpath ${DEPLOY_PATH} ${DEPLOY_PATH}_backup
rsync -a --stats . ${DEPLOY_PATH} --exclude .git

touch .env
echo "DISCORD_TOKEN=${DISCORD_TOKEN}" >> .env
echo "DISCORD_PUBLIC_KEY=${DISCORD_PUBLIC_KEY}" >> .env
echo "DISCORD_APP_ID=${DISCORD_APP_ID}" >> .env
echo "TEST_GUILD_ID=${TEST_GUILD_ID}" >> .env
cp .env ${DEPLOY_PATH}

pm2 restart ${PM2_NAME}