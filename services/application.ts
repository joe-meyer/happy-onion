import { REST } from "@discordjs/rest";
import { Client } from "discord.js";
const { Routes } = require('discord-api-types/v9');

export class Application {

    rest: REST;
    client: Client;
    commands: any = [];    
    clientId = process.env.DISCORD_APP_ID
    guildId = process.env.TEST_GUILD_ID
    constructor(rest: REST, client: Client) {
            this.rest = rest;
            this.client = client;            
            this.commands = [];
    }    

    async registerCommands() {        
        //register against the test guild
        const jsonCommands = this.commands.map(command => command.toJSON());
        if (this.guildId) {
            await this.rest.put(Routes.applicationGuildCommands(this.clientId, this.guildId), { body: jsonCommands })
            .then(() => console.log('Successfully registered application commands.'))
            .catch(console.error);
        }

        //registers comamnds against all guilds
        await this.rest.put(
            Routes.applicationCommands(this.clientId),
            { body: jsonCommands },
        );
    }
}