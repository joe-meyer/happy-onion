import { Application } from "../services/application";

module.exports = function(application: Application) {
    const { SlashCommandBuilder } = require('@discordjs/builders');
    const commandKey = "ping"

    application.commands.push(new SlashCommandBuilder().setName(commandKey).setDescription('Replies with pong!'))
    
    application.client.on('interactionCreate', async interaction => {
        if (!interaction.isCommand()) return;

        const { commandName } = interaction;

        if (commandName === commandKey) {
            interaction.reply(`Pong: ${application.client.ws.ping}ms.`);
        }
    });

    
}