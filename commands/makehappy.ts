import { Application } from "../services/application";

module.exports = function(application: Application) {
    const { SlashCommandBuilder } = require('@discordjs/builders');
    const commandKey = "makehappy";

    application.commands.push(
        new SlashCommandBuilder()
            .setName(commandKey)
            .setDescription('Makes onion happy!')
            .addStringOption(option => option.setName('message').setDescription("The message to send").setRequired(true))
            .addUserOption(option => option.setName('target').setDescription('The user to send the message').setRequired(true))
            .addIntegerOption(option => option.setName('number_of_messages').setDescription('The number of messages to send'))
            .addIntegerOption(option => option.setName('message_interval').setDescription('How often, in minutes, to send a message'))
    );
    
    application.client.on('interactionCreate', async interaction => {
        if (!interaction.isCommand()) return;

        const { commandName } = interaction;

        if (commandName === commandKey) {
            const userToMessage = interaction.options.getUser('target',true);      
            const messageToSend = interaction.options.getString('message', true);
            const messageCount = interaction.options.getInteger('number_of_messages', false) || 1;
            const messageInterval = interaction.options.getInteger('message_interval', false) || 1;                                 
            const usernameToSendTo = userToMessage.username;
          
            const userChannel = await userToMessage.createDM();
            let currentMessage = 0;
            const myInterval = setInterval(async () => {
                await userChannel.send(`${messageToSend} From ${interaction.member?.user.username}`);
                if (currentMessage++ >= messageCount) {
                    clearInterval(myInterval);
                }
            }, messageInterval * 60000);
            await interaction.reply(`Sending ${messageCount} messages every ${messageInterval} minute(s) to ${usernameToSendTo}.`);
        }
    });

    

    
}