import { Application } from "../services/application";
import * as fs from 'fs';
import * as path from 'path';

module.exports = function(application: Application){

    const commandFiles = fs.readdirSync(__dirname).filter(file => file.endsWith('.js') && file != 'commands.js' && file != 'register.js');
    
    for (const file of commandFiles) {
        const filePath = path.join(__dirname, file);        
        require(filePath)(application)
    }
}